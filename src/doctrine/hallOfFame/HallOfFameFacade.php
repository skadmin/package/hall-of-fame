<?php

declare(strict_types=1);

namespace Skadmin\HallOfFame\Doctrine\HallOfFame;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class HallOfFameFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = HallOfFame::class;
    }

    public function create(string $name, bool $isActive, string $title, ?string $imagePreview): HallOfFame
    {
        return $this->update(null, $name, $isActive, $title, $imagePreview);
    }

    public function update(?int $id, string $name, bool $isActive, string $title, ?string $imagePreview): HallOfFame
    {
        $hallOfFame = $this->get($id);
        $hallOfFame->update($name, $isActive, $title, $imagePreview);

        if (! $hallOfFame->isLoaded()) {
            $hallOfFame->setSequence($this->getValidSequence());
        }

        $this->em->persist($hallOfFame);
        $this->em->flush();

        return $hallOfFame;
    }

    public function get(?int $id = null): HallOfFame
    {
        if ($id === null) {
            return new HallOfFame();
        }

        $hallOfFame = parent::get($id);

        if ($hallOfFame === null) {
            return new HallOfFame();
        }

        return $hallOfFame;
    }

    /**
     * @return HallOfFame[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
