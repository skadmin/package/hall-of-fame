<?php

declare(strict_types=1);

namespace Skadmin\HallOfFame\Doctrine\HallOfFame;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class HallOfFame
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Title;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;

    public function update(string $name, bool $isActive, string $title, ?string $imagePreview): void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
        $this->title    = $title;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }
}
