<?php

declare(strict_types=1);

namespace Skadmin\HallOfFame;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'hall-of-fame';
    public const DIR_IMAGE = 'hall-of-fame';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-chess-rook']),
            'items'   => ['overview'],
        ]);
    }
}
