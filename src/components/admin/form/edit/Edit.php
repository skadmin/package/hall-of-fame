<?php

declare(strict_types=1);

namespace Skadmin\HallOfFame\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\HallOfFame\BaseControl;
use Skadmin\HallOfFame\Doctrine\HallOfFame\HallOfFame;
use Skadmin\HallOfFame\Doctrine\HallOfFame\HallOfFameFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory    $webLoader;
    private HallOfFameFacade $facade;
    private HallOfFame       $hallOfFame;
    private ImageStorage     $imageStorage;

    public function __construct(?int $id, HallOfFameFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->hallOfFame = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->hallOfFame->isLoaded()) {
            return new SimpleTranslation('hall-of-fame.edit.title - %s', $this->hallOfFame->getName());
        }

        return 'hall-of-fame.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->hallOfFame->isLoaded()) {
            if ($identifier !== null && $this->hallOfFame->getImagePreview() !== null) {
                $this->imageStorage->delete($this->hallOfFame->getImagePreview());
            }

            $hallOfFame = $this->facade->update(
                $this->hallOfFame->getId(),
                $values->name,
                $values->is_active,
                $values->title,
                $identifier
            );
            $this->onFlashmessage('form.hall-of-fame.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $hallOfFame = $this->facade->create(
                $values->name,
                $values->is_active,
                $values->title,
                $identifier
            );
            $this->onFlashmessage('form.hall-of-fame.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $hallOfFame->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->hallOfFame = $this->hallOfFame;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.hall-of-fame.edit.name')
            ->setRequired('form.hall-of-fame.edit.name.req');
        $form->addText('title', 'form.hall-of-fame.edit.title')
            ->setRequired('form.hall-of-fame.edit.title.req');
        $form->addCheckbox('is_active', 'form.hall-of-fame.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('image_preview', 'form.hall-of-fame.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.hall-of-fame.edit.send');
        $form->addSubmit('send_back', 'form.hall-of-fame.edit.send-back');
        $form->addSubmit('back', 'form.hall-of-fame.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->hallOfFame->isLoaded()) {
            return [];
        }

        return [
            'name'      => $this->hallOfFame->getName(),
            'title'     => $this->hallOfFame->getTitle(),
            'is_active' => $this->hallOfFame->isActive(),
        ];
    }
}
