<?php

declare(strict_types=1);

namespace Skadmin\HallOfFame\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\HallOfFame\BaseControl;
use Skadmin\HallOfFame\Doctrine\HallOfFame\HallOfFame;
use Skadmin\HallOfFame\Doctrine\HallOfFame\HallOfFameFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private HallOfFameFacade $facade;

    public function __construct(HallOfFameFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'hall-of-fame.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.hall-of-fame.overview.name')
            ->setRenderer(function (HallOfFame $hallOfFame): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $hallOfFame->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($hallOfFame->getName());

                return $name;
            });
        $grid->addColumnText('title', 'grid.hall-of-fame.overview.title');
        $this->addColumnIsActive($grid, 'hall-of-fame.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.hall-of-fame.overview.name');
        $grid->addFilterText('title', 'grid.hall-of-fame.overview.title');
        $this->addFilterIsActive($grid, 'hall-of-fame.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.hall-of-fame.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.hall-of-fame.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['sequence' => 'ASC']);

        return $grid;
    }
}
