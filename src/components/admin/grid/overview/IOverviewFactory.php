<?php

declare(strict_types=1);

namespace Skadmin\HallOfFame\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
