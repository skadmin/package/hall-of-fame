<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210313163234 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'hall-of-fame.overview', 'hash' => '4f76d6a5605d59fc7ecc5b695dceed51', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Síň slávy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.hall-of-fame.title', 'hash' => 'aba82aa30c41e5113f1aac9d054a4530', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Síň slávy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.hall-of-fame.description', 'hash' => 'b08e8714f76643cbeae897e33c6bd473', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat členy síně slávy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'hall-of-fame.overview.title', 'hash' => '44e850bc994bc86dd667a105c3697142', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Síň slávy|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.hall-of-fame.overview.is-active', 'hash' => '1ff3fdd0caf6e21938dbde2e7db8d91e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.hall-of-fame.overview.action.new', 'hash' => 'f09b517aa32bb3c400e42ae9aa9111fc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit člena', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.hall-of-fame.overview.name', 'hash' => 'f9b1b48db66ba5c7cebd4806a9031c1d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.hall-of-fame.overview.title', 'hash' => '7872bf3de10cc6ce0586be3e9987cf89', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titul', 'plural1' => '', 'plural2' => ''],
            ['original' => 'hall-of-fame.edit.title', 'hash' => 'c37cd50640b18b6793b2972bb45dd19b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení člena síně slávy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.name', 'hash' => 'af1890e6b72ae3c7544f96ea99d6984c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.name.req', 'hash' => 'ff6fed8de7f2fe399fba9cbe693bd771', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.title', 'hash' => '5066a2d4a616224939b656ddc9aaa96a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titul', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.title.req', 'hash' => '90c63d3beefc046b9d0cfbad3ec339f6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím titul', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.image-preview', 'hash' => 'd8018a2d27b9cc2cd9101a446865ccae', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled člena síně slávy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.image-preview.rule-image', 'hash' => '36759a581cfd9f6cfea7915a7bc29a9f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.is-active', 'hash' => '06b1d29810e1844c81993637a5c363f0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.send', 'hash' => '43eb05723d0ae671d43bbdf00313e99e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.send-back', 'hash' => 'e50a4a4f10eee4045990c9a7e3eb5fdf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.back', 'hash' => 'facb7cd7bdcd125cc30e5e5b4a4519e9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'hall-of-fame.edit.title - %s', 'hash' => '6bc2f30ff2c873c4779f58db5d581c38', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace člena síně slávy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.flash.success.create', 'hash' => 'b674f4e36ea17b2a6aa88824d1133cba', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Člen síně slávy byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.hall-of-fame.edit.flash.success.update', 'hash' => '6aaaebb901618c78ff3ad4b6352406eb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Člen síně slávy byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.hall-of-fame.overview.action.edit', 'hash' => '7e02bb6df8b445d4d59caf67c0e0c5c1', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
